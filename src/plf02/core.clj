(ns plf02.core)

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 [(list 1 2 3) (list 4 5 6)])
(función-associative?-2 {\. \* \$ \%})
(función-associative?-3 #{10 20 30 40 50})

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 true)
(función-boolean?-2 false)
(función-boolean?-3 5)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 2)
(función-char?-1 (first #{\a \b \c \d}))
(función-char?-1 "a")


(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [#{} [] (list) (vector) (hash-set)])
(función-coll?-2 (hash-set))
(función-coll?-3 10)

(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 100M)
(función-decimal?-2 1.333)
(función-decimal?-3 0.333M)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 1000)
(función-double?-1 0.0000000000000000000000000002)
(función-double?-3 100.000001)

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 2.0)
(función-float?-2 10000.10000001)
(función-float?-3 100M)

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 :cecy)
(función-ident?-1 "cecy")
(función-ident?-1 'cecy)

(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [1 2 3 4 5 6 7 8 {10 20 30 40} (list 50 60 70)])
(función-indexed?-2 #{10 20})
(función-indexed?-3 {:1 10 :2 20 :3 30 :4 40})


(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 10000)
(función-int?-2 0.1)
(función-int?-3 10)


(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 2)
(función-integer?-2 10)
(función-integer?-3 2.0)


(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :a)
(función-keyword?-2 'cecy)
(función-keyword?-3 "cecy")


(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 (list 20 30 40))
(función-list?-2 '())
(función-list?-3 [1 2 3 4 5 6 7 8 {10 20 30 40} (list 50 60 70)])


(defn map-entry?-1
  [a]
  (map-entry? a))

(defn map-entry?-2
  [a]
  (map-entry? a))

(defn map-entry?-3
  [a]
  (map-entry? a))

(map-entry?-1 (first {:a 1 :b 2}))
(map-entry?-2 (last {:a 1 :b 2}))
(map-entry?-3 10)


(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {:nombre "cecy" :apellido "lopez" :edad 10 :sexo "femenino"})
(función-map?-2 (hash-map :1 10 :2 20 :3 30 :4 40))
(función-map?-3 [1 2 3 4])


(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 100)
(función-nat-int?-2 0.1)
(función-nat-int?-3 100.0)


(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 100)
(función-number?-2 "hola")
(función-number?-3 \a)


(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 10)
(función-pos-int?-2 -10)
(función-pos-int?-3 10.0)


(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 10/3)
(función-ratio?-2 -1/4)
(función-ratio?-3 1000)


(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 1)
(función-rational?-2 1.0)
(función-rational?-3 10/9)

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 '(1 10 22 1 23))
(función-seq?-2 (range 10 40))
(función-seq?-3 {10 20 30 40})


(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 nil)
(función-seqable?-2 "cecy")
(función-seqable?-3 [1 2 3 4])


(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 [10 20])
(función-sequential?-2 {\a \b \v \v})
(función-sequential?-3 #{1/2 1/3 1/4 1/5})


(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 (list 1 2))
(función-set?-2 (hash-map 10 20 30 40))
(función-set?-3 (hash-set 40 50))


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 [\a \b])
(función-some?-2 #{10 20})
(función-some?-3 nil)


(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "Emanuel")
(función-string?-2 \a)
(función-string?-3 "a")


(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 'simbolo)
(función-symbol?-2 "a")
(función-symbol?-3 'x)


(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 [\. \. \. \. \.])
(función-vector?-2 {1 2 3 4})
(función-vector?-3 (list 0 0 0 0))


(defn función-drop-1
  [n as]
  (drop n as))

(defn función-drop-2
  [n as]
  (drop n as))

(defn función-drop-3
  [n as]
  (drop n as))

(función-drop-1 5 #{[\a \b ] (list 1 2 3) [10 20] {1 2 3 4} [1/2 1/4] #{10 100} [0.0 0.1 0.2]})
(función-drop-2 2 [1 2 3 4 5 6 5])
(función-drop-3 3 (hash-map :a 10 :b 20 :c 30 :d 40 :e 50 :f 60))


(defn función-drop-last-1
  [n as]
  (drop-last n as))

(defn función-drop-last-2
  [n as]
  (drop-last n as))

(defn función-drop-last-3
  [as]
  (drop-last as))

(función-drop-last-1 2 (list [\a \b] (list 1 2 3) [10 20] {1 2 3 4} [1/2 1/4] #{10 100} [0.0 0.1 0.2]))
(función-drop-last-2 1 (vector 1 2 3 4 0))
(función-drop-last-3 (hash-set 10 20 30 40 50))


(defn función-drop-while-1
  [pred as]
  (drop-while pred as))

(defn función-drop-while-2
  [pred as]
  (drop-while pred as))

(defn función-drop-while-3
  [pred as]
  (drop-while pred as))
  
(función-drop-while-1 (fn [x] (neg? x)) [3 4 5 6 -7 8])
(función-drop-while-2 (fn [x] (pos? x)) [3 4 5 6 -7 8])
(función-drop-while-3 (fn [x] (number? x)) [3 4 5 6 -7 8])


(defn función-every?-1
  [p as]
  (every? p as))

(defn función-every?-2
  [p as]
  (every? p as))

(defn función-every?-3
  [p as]
  (every? p as))

(función-every?-1 (fn [x] (pos? x)) [2 4 6 8])
(función-every?-2 (fn [x] (string? x)) ["a" "b" "c"])
(función-every?-3 (fn [x] (int? x)) (list 2 4 -6 8 10 20 -30 40 50))


(defn función-filterv?-1
  [n as]
  (filterv n as))

(defn función-filterv?-2
  [n as]
  (filterv n as))

(defn función-filterv?-3
  [n as]
  (filterv n as))

(función-filterv?-1 (fn [x] (even? x)) [1 2 3 4 5 6 7 8])
(función-filterv?-2 (fn [x] (== (count x) 3)) [[1 2 3 4][1 2 3][\b][10 20 30][\a][60 70 80 90 100]])
(función-filterv?-3 (fn [x] (int? x)) #{10 30 \a \b 1 \c 1.1 \g 5})


(defn función-group-by-1
  [n as]
  (group-by n as))

(defn función-group-by-2
  [ f as]
    (group-by f as))

(defn función-group-by-3
  [n as]
  (group-by n as))

(función-group-by-1 (fn [x] (even? x)) [1 2 3 4])
(función-group-by-2 :nombre [{:nombre "Evelin" :edad 15}{:nombre "Yuliana" :edad 8} {:nombre "ana" :edad 10} {:nombre "rosa" :edad 12} {:nombre "emma" :edad 2}])
(función-group-by-3 (fn [x] (== (count x) 3)) [[1 1 1 ][2 3 4 5 6][5 5 5 ][6 7 8 1][10 22][11][16 17 18][0]])

(defn función-iterate-1
  [f x]
  (iterate f x))

(defn función-iterate-2
  [f x]
  (iterate f x))

(defn función-iterate-3
  [f x]
  (iterate f x))

(función-iterate-1 inc 5)
(función-iterate-2 dec 2)
(función-iterate-3 inc 2)

(defn función-keep-1
  [f xs]
  (keep f xs))

(defn función-keep-2
  [f xs]
  (keep f xs))

(defn función-keep-3
   [f xs]
  (keep f xs))

(función-keep-1 :a '({:a 1} {:b 3} {:a 8} {:z 7}))
(función-keep-2 (fn [x] (== (count x) 3)) [[1 1 1] [2 3 4 5 6] [5 5 5] [6 7 8 1] [10 22] [11] [16 17 18] [0]])
(función-keep-3 (fn [x] (integer? x)) [\a \b \c 1 2 3 4 \d \e])

(defn función-keep-indexed-1
  [f xs]
   (keep-indexed f xs))

(defn función-keep-indexed-2
  [f xs]
   (keep-indexed f xs))

(defn función-keep-indexed-3
  [f xs]
   (keep-indexed f xs))

(función-keep-indexed-1 (fn [index v] (if (even? v) index)) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-2 (fn [index v] (if (pos? v) index)) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 (fn [index v] (if (neg? v) index)) [-9 0 29 -7 45 3 -8])

(defn función-map-indexed-1
  [f xs]
  (map-indexed f xs ))

(defn función-map-indexed-2
  [f xs]
  (map-indexed f xs))

(defn función-map-indexed-3
  [f xs]
  (map-indexed f xs))

(función-map-indexed-1 (fn [index v] [index v]) "cecilia")
(función-map-indexed-2 (fn [index v] [index v]) {"a" "b"})
(función-map-indexed-3 hash-set (list 10 20 30 40 50 60 70 80 90))


(defn función-mapcat-1 
  [f as] 
  (mapcat f as))

(defn función-mapcat-2 
  [f as] 
  (mapcat f as))

(defn función-mapcat-3 
  [f as] 
  (mapcat f as))

(función-mapcat-1 (fn [x] (range x))[10 2 3 4])
(función-mapcat-2 (fn [x] (range x)) [10 2 3 1 2 3 4 5 6 4])
(función-mapcat-3 (fn [x] (range x)) [10 2])


(defn función-mapv-1 
  [ f as bs] 
  (mapv  f as bs))

(defn función-mapv-2 
  [f as] 
  (mapv f as))

(defn función-mapv-3 
  [f as bs cs] 
  (mapv f as bs cs))

(función-mapv-1 (fn [x y] (+ x y)) [10 20 30] [40 50 60])
(función-mapv-2 (fn [x] (pos? x)) [-9 8 7 -6 5 -4 3 2 1 0])
(función-mapv-3 (fn [x y z] (* x y z)) [-9 8 7 -6 5 ][-4 3 2 1 0][1 2 3 4 5])

(defn función-merge-with-1 
  [f as bs ] 
  (merge-with f as bs))

(defn función-merge-with-2 
  [f as bs cs ] 
  (merge-with f as bs cs ))

(defn función-merge-with-3 
  [f as bs  cs ds] 
  (merge-with f as bs cs ds))

(función-merge-with-1 (fn [x y] (+ x y)) {:a 1  :b 2} {:a 9  :b 98  :c 0})
(función-merge-with-2 (fn [x y] (* x y)) {:a 1  :b 2} {:a 9  :b 98  :c 0} {:a 9  :b 98  :c 0})
(función-merge-with-3 (fn [x y] (- x y)) {:a 1  :b 2} {:a 9  :b 98  :c 10} {:a 9  :b 98  :c 1}  {:a 9  :b 98  :c 2})


(defn función-not-any?-1 
  [n ns] 
  (not-any? n ns))

(defn función-not-any?-2 
  [n ns] 
  (not-any? n ns))

(defn función-not-any?-3 
  [n ns] 
  (not-any? n ns))

(función-not-any?-1 (fn [x] (odd? x) )[10 20 30 40 50 60])
(función-not-any?-2 (fn [x] (>= x 3)) [3 5 6 7 10 20 30 40])
(función-not-any?-3 (fn [x] (== x 10)) [10 10 10])


(defn función-not-every?-1 
  [n ns] 
  (not-every? n ns))

(defn función-not-every?-2 
  [n ns] 
  (not-every? n ns))
                            
(defn función-not-every?-3 
  [n ns] 
  (not-every? n ns))

(función-not-every?-1 (fn [x] (<= x 1)) [4 5 6 7 1 2 3 4])
(función-not-every?-1 (fn [x] (>= x 10)) [4 5 6 7 1 2 3 4 10 20])
(función-not-every?-1 (fn [x] (>= x 5)) [4 5 6 7 1])

(defn función-partition-by-1 
  [f zs] 
  (partition-by f zs))

(defn función-partition-by-2 
  [n ns] 
  (partition-by n ns))

(defn función-partition-by-3 
  [n ns] 
  (partition-by n ns))

(función-partition-by-1 (fn [x] (odd? x)) [10 11 20 30 40 41 43 45 50 60 70])
(función-partition-by-2 (fn [x] (>= x 1)) (vector 1 2 1 2 2 3 3))
(función-partition-by-3 (fn [x] (== x 3)) (list 10 10 10 20 20 20 3 4 2))


(defn función-remove-1 
  [f xs] 
  (remove f xs))

(defn función-remove-2 
  [f xs] 
  (remove f xs))

(defn función-remove-3 
  [p xs] 
  (remove p xs))

(función-remove-1 (fn [x] (== x 1)) [2 3 1 3 3 4 5 6 6 1 1])
(función-remove-2 (fn [x] (pos? x)) [-2 3 -1 3 3 -4 5 -6 -6 1 -1])
(función-remove-3 (fn [x] (odd? x)) [2 3 1 3 3 4 5 6 6 1 1])

(defn función-reverse-1 
  [xs] 
  (reverse xs))

(defn función-reverse-2 
  [xs] 
  (reverse xs))

(defn función-reverse-3 
  [xs] 
  (reverse xs))

(función-reverse-1 "cecilia")
(función-reverse-2 #{10 20 30 40})
(función-reverse-3 (list \a \b \c \d \e \f))

(defn función-some-1 
  [n xs] 
  (some n xs))

(defn función-some-2 
  [p xs] 
  (some p xs))

(defn función-some-3 
  [p xs] 
  (some p xs))

(función-some-1 (fn [x] (== x 10)) [20 10 30 20 5 6 7 8])
(función-some-2 (fn [x] (even? x)) (vector 2 3 4 5 6 7 8 9 10 11 11))
(función-some-3 (fn [x] (neg-int? x)) #{10 20 30 40 50})


(defn función-sort-by-1 
  [f xs ] 
  (sort-by f xs))

(defn función-sort-by-2 
  [f xs ] 
  (sort-by f xs))

(función-sort-by-1 :edad [{:nombre "Evelin" :edad 15}{:nombre "Yuliana" :edad 8} {:nombre "ana" :edad 10}{:nombre "rosa" :edad 12}{:nombre "emma" :edad 2}])
(función-sort-by-2 :nombre [{:nombre "Evelin" :edad 15} {:nombre "Yuliana" :edad 8} {:nombre "Ana" :edad 10}{:nombre "Rosa" :edad 12} {:nombre "Emma" :edad 2}])


(defn función-split-with-1 
  [n xs] 
  (split-with n xs))

(defn función-split-with-2 
  [p xs] 
  (split-with  p xs))

(defn función-split-with-3 
  [p xs] 
  (split-with  p xs))

(función-split-with-1 (fn [x] (== x 2)) [2 2 2 2 10 20 30 5 10 10 20 20 ])
(función-split-with-2 (fn [x] (pos? x)) (vector 10 20 -4 5 6 7 8 -9 -9))
(función-split-with-3 (fn [x] (even? x)) #{10 20 30 3 5 7 9})


(defn función-take-1 
  [n xs]
  (take n xs))

(defn función-take-2 
  [n xs] 
  (take n xs))

(defn función-take-3 
  [n xs] 
  (take n xs))

(función-take-1 3 (hash-set 2 3 4 5 6 7 8 9 0))
(función-take-2 1 (hash-map 1 10 2 20 3 30 4 40))
(función-take-3 0 '(1 2 3 4))


(defn función-take-last-1 
  [n xs] 
  (take-last n xs))

(defn función-take-last-2 
  [n xs] 
  (take-last n xs))

(defn función-take-last-3 
  [n xs]
  (take-last n xs))

(función-take-last-1 3 (hash-set 2 3 4 5 6 7 8 9 0))
(función-take-last-2 1 [1 10 2 20 3 30 4 40])
(función-take-last-3 4 '(1 2 3 4))

(defn función-take-nth-1 
  [n xs] 
  (take-nth n xs))

(defn función-take-nth-2 
  [n xs]
  (take-nth n xs))

(defn función-take-nth-3 
  [n xs] 
  (take-nth n xs))

(función-take-nth-1 4 #{ \a \b \c \d \e})
(función-take-nth-2 2 (range 1 21 2))
(función-take-nth-3 1 (vector 1 2 3 0))


(defn función-take-while-1 
  [n xs] 
  (take-while n xs))

(defn función-take-while-2 
  [n xs] 
  (take-while n xs))

(defn función-take-while-3 
  [n xs] 
  (take-while n xs))

(función-take-while-1 (fn [x] (== x 2)) [2 2 2 2 10 20 30 5 10 10 20 20])
(función-take-while-2 (fn [x] (pos? x)) (vector 10 20 -4 5 6 7 8 -9 -9))
(función-take-while-3 (fn [x] (>= x 10)) (list 10 20 30 3 5 7 9))


(defn función-update-1 
  [a b c ]
  (update a b c ))

(defn función-update-2 
  [a b c]
  (update a b c))

(defn función-update-3 
  [a b c]
  (update a b c))

(función-update-1 {:a 10 :b 20 :c 30 :d 40} :a inc)
(función-update-1 {:a 10 :b 20 :c 30 :d 40} :b dec)
(función-update-1 {:a 10 :b 20 :c 30 :d 40} :c dec)

(defn función-update-in-1 
  [a b c]
  (update-in a b c))

(defn función-update-in-2 
  [a b c]
  (update-in a b c))

(defn función-update-in-3 
  [a b c]
  (update-in a b c))

(función-update-in-1 [{:nombre "Ema" :edad 10} {:nombre "Cecy" :edad 11}]  [1 :edad] inc)
(función-update-in-2 [{:nombre "Ema" :edad 10} {:nombre "Cecy" :edad 11}] [1 :edad] dec)
(función-update-in-3 [{:nombre "Ema" :edad 10} {:nombre "Cecy" :edad 11}] [0 :edad] dec)

